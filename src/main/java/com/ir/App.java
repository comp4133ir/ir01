package com.ir;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Hello world!
 *
 */
public class App {
	public static void main(String[] args) {
//		System.out.println("Hello World!");
		Logger log = LoggerFactory.getLogger(App.class);
		log.info("HelloWorld");
		log.debug("HelloWorld!");
	}
}
